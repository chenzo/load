"""
    Reads the configuration file and loads the parameters.

    @param conf_filename The path to the configuration file.

    @return A dictionnary containing the parameter loaded.
"""
def load_conf(conf_filename):
    conf = {}
    with open(conf_filename) as conf_file:
        for line in conf_file:
            key, value = line.split(":", 1)
            conf[key] = value.strip()
    return conf
