#!/usr/bin/python

from utils import *

from datetime import *
import os

class Todo:
    def __init__(self, label, time_spent, duration, index=-1):
        self.label = label
        self.time_spent = time_spent
        self.duration = duration
        self.index = index

    def print_string(self):
        f = "{3}) {0} [{1} / {2}]"
        return f.format(
                    self.label,
                    self.time_spent,
                    self.duration,
                    self.index)

    def __str__(self):
        f = "{0};{1};{2};{3}"
        return f.format(
                    self.label,
                    self.time_spent,
                    self.duration,
                    self.index)

class Todos:
    def __init__(self, conf_filename):
        conf = load_conf(conf_filename)
        self.filename = conf["data"]
        self.todos = {}
        self.read_file()

        self.free_days = {}
        free_days_filename = conf["free_days"]
        holidays_filename = conf["holidays"]
        self.load_free_days(free_days_filename)
        self.load_free_days(holidays_filename)

        self.reserved_days = load_conf(conf["reserved_days"])

        self.cmds = {
                "": (self.cmd_help, 
                    None,
                    None),
                "a": (self.add,
                    "a label duration [index] [time_spent]",
                    "Adds a new todo item."),
                "h": (self.cmd_help,
                    "h",
                    "Show this message"),
                "md": (self.modify_duration,
                    "md index new_duration",
                    "Modifies todo duration."),
                "ml": (self.modify_label,
                    "ml index new_label",
                    "Modifies todo label."),
                "mt": (self.modify_time_spent,
                    "mt index new_time_spent",
                    "Modifies todo time_spent."),
                "pd": (self.print_date_list,
                    "pd [length]",
                    "Prints todo list with calendar date."),
                "p": (self.print_list,
                    "p [length]",
                    "Prints todo list."),
                "r": (self.remove,
                    "r index",
                    "Removes a specified todo item."),
                "s": (self.switch,
                    "s index new_index",
                    "Switches item position.")
        }
        self.post_cmds = {
            "a": self.write,
            "md": self.write,
            "ml": self.write,
            "mt": self.write,
            "r": self.write,
            "s": self.write
        }

    def __str__(self):
        todo_str = ""
        for todo in self.todos:
            todo_str += str(self.todos[todo]) + "\n"
        return todo_str

    def add(self, label, duration, index=-1, time_spent=0):
        index = int(index)
        time_spent = float(time_spent)

        if(index == -1 or index > len(self.todos)):
            index = len(self.todos)

        # moves todos with greater index
        tmp_index = len(self.todos)
        while(tmp_index > index):
            self.todos[tmp_index] = self.todos[tmp_index-1]
            self.todos[tmp_index].index = tmp_index
            tmp_index -= 1

        # TODO check if already exists.
        self.todos[index] = Todo(label, time_spent, duration, index)
        return self.todos[index].print_string()

    def load_free_days(self, filename):
        with open(filename, "r") as fd:
            for line in fd:
                self.free_days[line.strip()] = True


    def modify_duration(self, index, duration):
        index = int(index)
        duration = float(duration)

        if(index >= 0 and index < len(self.todos)):
            self.todos[index].duration = duration
            return self.todos[index].print_string()

    def modify_label(self, index, label):
        index = int(index)

        if(index >= 0 and index < len(self.todos)):
            self.todos[index].label = label
            return self.todos[index].print_string()

    def modify_time_spent(self, index, time_spent):
        index = int(index)
        time_spent = float(time_spent)

        if(index >= 0 and index < len(self.todos)):
            self.todos[index].time_spent = time_spent
            return self.todos[index].print_string()

    def remove(self, index):
        index = int(index)

        while(index < len(self.todos)-1):
            self.todos[index] = self.todos[index+1]
            self.todos[index].index = index
            index += 1
        # The item to remove is already squeezed. Thus just removes the last
        # item that the previous loop has dupplicated.
        del(self.todos[index])

    def read_file(self):
        with open(self.filename, "a+") as fd:
            fd.seek(0)
            for line in fd:
                label, time_spent, duration , index = line.split(";")
                index = int(index.strip())
                time_spent = float(time_spent.strip())
                duration = float(duration.strip())
                label = label.strip()
                self.add(label, duration, index, time_spent)

    def print_date_list(self, length=-1):
        length = int(length)
        todo_str = ""

        if(length == -1):
            length = len(self.todos)
        index = 0
        today = date.today()
        delta_date = timedelta(days=0)
        one_day = timedelta(days=1)
        time_to_spend = 0

        # Skip days if we are currently in a weekend or in a free day.
        delta_date += self.get_nb_skip_days(today, delta_date)

        while(index < length):
            todo_str += "{"+str(today + delta_date)+"} " + \
                         self.todos[index].print_string() + "\n"
            time_to_spend = \
                self.todos[index].duration - self.todos[index].time_spent
            next_delta_date = delta_date + timedelta(days=time_to_spend)
            # TODO: Manages days to skip.
            while(delta_date <= next_delta_date):
                # Skip free days
                nb_skip_days = self.get_nb_skip_days(today, delta_date)
                delta_date += nb_skip_days
                next_delta_date += nb_skip_days
                delta_date += one_day
            # Set in order to keep the hours or minutes remaining.
            delta_date = next_delta_date
            index += 1
        return todo_str

    def print_list(self, length=-1):
        length = int(length)
        todo_str = ""

        if(length == -1):
            length = len(self.todos)
        index = 0
        while(index < length):
            todo_str += self.todos[index].print_string() + "\n"
            index += 1
        return todo_str

    def get_nb_skip_days(self, today, delta_date):
        one_day = timedelta(days=1)
        # Skip free days
        nb_skip_days = timedelta(days=0)
        tmp_date = today + delta_date + nb_skip_days
        while(self.reserved_days.get(str(tmp_date.weekday()), None) != None \
                or self.free_days.get(str(tmp_date))):
            nb_skip_days += one_day
            tmp_date = today + delta_date + nb_skip_days
        return nb_skip_days

    def switch(self, index, new_index):
        index = int(index)
        new_index = int(new_index)

        if(new_index < 0):
            new_index = 0
        elif(new_index > len(self.todos) -1):
            new_index = len(self.todos) - 1

        if(index >= 0 and index < len(self.todos)
                and index != new_index):

            tmp = self.todos[index]
            tmp.index = new_index
            self.remove(index)
            self.add(tmp.label, tmp.duration, tmp.index, tmp.time_spent)
            return tmp.print_string()


    def write(self):
        swap_filename = self.filename + ".swap"
        self.write_file(swap_filename)
        os.rename(swap_filename, self.filename)

    def write_file(self, filename):
        with open(filename, "w") as fd:
            fd.write(str(self))

    """
        Prints the list of available commands.
    """
    def cmd_help(self, *args, **kwargs):
        res = "Command list help:\n"
        for cmd in sorted(self.cmds.keys()):
            cmd_tuple = self.cmds[cmd]
            if cmd_tuple[1] != None:
                res += "- " + cmd_tuple[1] + "\n\t" + cmd_tuple[2] + "\n"
        return res

    """
        Parses and calls the corresponding function.
    """
    def parse_and_call_cmd(self, user_input):
        result = ""

        cmd = self.cmds.get(user_input[0], None)
        if(cmd != None and cmd[0] != None):
            result = cmd[0](*user_input[1:])
            # post action
            post_cmd = self.post_cmds.get(user_input[0], None)
            if(post_cmd != None):
                post_cmd()
        return result

