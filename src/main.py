#!/usr/bin/python3

import sys

from load import *

"""
    Main function.
"""
if __name__ == "__main__":
    conf_filename = "conf/.conf"
    todos = Todos(conf_filename)
    #print("total: " + str(todos.get_total()))
    #todos.write()

    # Parse and execute the given arguments.
    if len(sys.argv) > 1:
        user_input = sys.argv[1:]
        res = todos.parse_and_call_cmd(user_input)
        if(res != None):
            print(res)
    # Run in console scheme.
    else:
        user_input = [""]
        while(user_input[0] != 'q'):
            # TODO do not parse space between quotes or doube quotes.
            user_input = input("\nCommand (h for help): ").split(" ")
            res = todos.parse_and_call_cmd(user_input)
            if(res != None):
                print(res)
